cd ./../..
mkdir seed-web
cd ./seed-web
git clone git@framagit.org:seed-server/main.git


git clone "git@framagit.org:seed-web/static.git"
git clone "git@framagit.org:seed-web/libraries.git"
git clone "git@framagit.org:seed-web/data.git"
git clone "git@framagit.org:seed-web/manager.git"
git clone "git@framagit.org:seed-web/editor.git"
git clone "git@framagit.org:seed-web/engine.git"
git clone "git@framagit.org:seed-web/view-large.git"
git clone "git@framagit.org:seed-web/view-medium.git"
git clone "git@framagit.org:seed-web/view-small.git"
git clone "git@framagit.org:seed-web/view-fullscreen.git"
