cd ./../..
mkdir seed-server
cd ./seed-server
git clone git@framagit.org:seed-server/main.git
git clone git@framagit.org:seed-server/test.git
git clone git@framagit.org:seed-server/installer.git
git clone git@framagit.org:seed-server/doc.git
git clone git@framagit.org:seed-server/dev.git

mkdir languages
cd ./languages
git clone git@framagit.org:seed-server/languages/python.git
git clone git@framagit.org:seed-server/languages/php.git
git clone git@framagit.org:seed-server/languages/js.git
git clone git@framagit.org:seed-server/languages/bash.git
git clone git@framagit.org:seed-server/languages/arduino.git

cd ..
mkdir services
cd ./services
git clone git@framagit.org:seed-server/services/ssh.git
git clone git@framagit.org:seed-server/services/nginx.git
git clone git@framagit.org:seed-server/services/git.git
git clone git@framagit.org:seed-server/services/docker.git
git clone git@framagit.org:seed-server/services/doc.git

cd ..
mkdir workshops
cd ./workshops

git clone git@framagit.org:seed-server/workshops/server.git
git clone git@framagit.org:seed-server/workshops/server.media.git

git clone git@framagit.org:seed-server/workshops/desktop.git
git clone git@framagit.org:seed-server/workshops/desktop.music.git
git clone git@framagit.org:seed-server/workshops/desktop.graphic.git
git clone git@framagit.org:seed-server/workshops/desktop.games.git
git clone git@framagit.org:seed-server/workshops/desktop.dev.git


cd ..
mkdir os
cd ./os

git clone git@framagit.org:seed-server/os/linux.git
git clone git@framagit.org:seed-server/os/gnome.git
git clone git@framagit.org:seed-server/os/debian.git
