cd "./../.."
mkdir "./seed-demos"
cd "./seed-demos"

git clone "git@framagit.org:seed-demos/vm.local.git"
git clone "git@framagit.org:seed-demos/vm.docker.git"
git clone "git@framagit.org:seed-demos/vm.distant.git"

git clone "git@framagit.org:seed-demos/docker.git-gogs"
git clone "git@framagit.org:seed-demos/docker.mattermost"
git clone "git@framagit.org:seed-demos/nodejs.react"
git clone "git@framagit.org:seed-demos/nodejs.webpack"
git clone "git@framagit.org:seed-demos/python.rdf"
git clone "git@framagit.org:seed-demos/pyxml.many-servers"
git clone "git@framagit.org:seed-demos/pyxml.simples_programs"
git clone "git@framagit.org:seed-demos/pyxml.term-anim"

