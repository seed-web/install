<?php
include("./templates/functions.php");
include("./templates/menubar.php");
?>
<link rel="stylesheet" href="./style.css">
<div class="content">
<?php


command("echo \$PATH 2>&1");
command("hostname 2>&1");
command("echo \$USER 2>&1");
command("uname -a 2>&1");
command("cat /proc/version 2>&1");
command("lsb_release -a 2>&1");
command("python3 -V 2>&1");
command("pip3 -V 2>&1");
command("php -v 2>&1");
command("composer -v 2>&1");
command("bash --version 2>&1");
command("npm -v 2>&1");
command("git --version 2>&1");
command("tree 2>&1");
command("ls -al 2>&1");
//command("apt search tree 2>&1");
command("apt install tree 2>&1");
?>
</div>
